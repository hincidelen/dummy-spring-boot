FROM maven:3.5.2-jdk-8-alpine AS build
COPY . /tmp/
WORKDIR /tmp/
RUN mvn -DskipTests=true package 

FROM openjdk:8-jdk-alpine
WORKDIR /tmp/
COPY --from=build tmp/target/helloworld*.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","app.jar"]
